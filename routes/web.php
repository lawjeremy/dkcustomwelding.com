<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return view('home');

});

$app->get('/restorations', function () use ($app) {
    return view('restorations');

});

$app->get('/laser-cutting', function () use ($app) {
    return view('laser');

});

$app->get('/home-upgrades', function () use ($app) {
    return view('home-upgrades');

});

$app->get('/custom-fabrication', function () use ($app) {
    return view('custom-fabrication');

});

$app->get('/gallery', function () use ($app) {
    return view('gallery');

});

$app->get('/about', function () use ($app) {
    return view('about');

});

$app->get('/contact', function () use ($app) {
    return view('contact');

});

$app->get('gallery/{type}', function ($type) {

    if(view()->exists('gallery.' . $type)){
      return view('gallery.' . $type);
    }
    else {
      return '404';
    }

});
