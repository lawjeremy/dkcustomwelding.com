({
    baseUrl: ".",
    "paths": {
      "underscore": "libs/underscore",
      "backbone": "libs/backbone",
      "jquery": "libs/jquery",
      "bootstrap": "libs/bootstrap.min",
      "masked-input": "libs/jquery.maskedinput",
      // "coverflow-responsive": "libs/FWDSimple3DCoverflow",
      // "jqueryui": "libs/jquery-ui.min",
      // "coverflow": "libs/coverflow",
      "flipster": "libs/jquery.flipster.min",
      "router": "router",
      "text": "libs/text"
    },
    "shim": {
      "masked-input": [ "jquery" ],
      "libs/carousel": [ "jquery" ]
    },
  excludeShallow: ["text"],
    name: "main",
    out: "dk.build.js"
})
