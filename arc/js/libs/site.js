/**
* Created with dkcustomwelding.com.
* User: lawjeremy
* Date: 2014-03-09
* Time: 11:48 AM
*/
define( [ 'jquery', 'underscore', 'backbone', 'bootstrap' ], function ( $, _, Backbone ) {
  
      $(document).ready(function () {
        
          initPage = function(){
            
            //$( '.home-hero' ).css ( 'height', window.innerHeight );

//             $( '.slider-link' ).bind ( 'click', function ( evt ) {
//               $ ( '.carousel' ).carousel ();
//             });

          }
          
          //bind event handlers
          $( document ).bind ( 'scroll', function () {

            var top = $( document ).scrollTop();
            var width = window.innerWidth;
            //console.log ( width );

            if ( top > 100 && width > 768 ) {

              $( 'header' ).addClass ( 'header-reduced' );
              $( '.gallery-sub-nav' ).addClass ( 'high' );
            }

            if ( top == 0 ) {

              $( 'header' ).removeClass ( 'header-reduced' ); 
              $( '.gallery-sub-nav' ).removeClass ( 'high' );
            }

          });
      });

       
return {};
});