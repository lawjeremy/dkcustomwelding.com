require.config ({
  "paths": {
    "underscore": "libs/underscore",
    "backbone": "libs/backbone",
    "jquery": "libs/jquery",
    "bootstrap": "libs/bootstrap.min",
    "masked-input": "libs/jquery.maskedinput",
    "router": "router",
    "text": "libs/text"
  }, 
  "shim": {
    "masked-input": [ "jquery" ],
    "libs/carousel": [ "jquery" ]
  }
  
});
  
require ( [ "jquery", "underscore", "backbone", "router" ], function ( $, _, Backbone, Router ) {
  
  $.fn.preload = function() {
      this.each(function(){
          $('<img/>')[0].src = this;
      });
  }

  // Usage:

  $(['/img/dk-liner-1.jpg','/img/dk-cover-7.png','/img/dk-tube-1.jpg']).preload();
  Router.initialize();
  
  
});