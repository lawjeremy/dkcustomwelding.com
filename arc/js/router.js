
define( [ 'jquery',
          'underscore',
          'backbone',
          'views/home',
          'views/about',
          'views/gallery',
          'views/contact',
          'views/products',
          'views/restorations',
          'libs/site' ], function( $,
                                   _,
                                   Backbone,
                                   HomeView,
                                   AboutView,
                                   GalleryView,
                                   ContactView,
                                   ProductView,
                                   RestorationView ) {

   var AppRouter = Backbone.Router.extend({
        routes: {
          "about": "aboutRoute",
          "gallery": "galleryRoute",
          "gallery/trucks": "galleryTrucksRoute",
          "gallery/home": "galleryHomeRoute",
          "gallery/custom": "galleryCustomRoute",
          "gallery/restoration": "galleryRestorationRoute",
          "gallery/laser": "galleryLaserRoute",
          "products": "productsRoute",
          "restorations": "restorationsRoute",
          "contact": "contactRoute",
          "*actions": "defaultRoute"
        }
    });

    var initialize = function () {
      // Initiate the router
      var app_router = new AppRouter;

      app_router.on ( 'route:defaultRoute', function () {

        var homeView = new HomeView();

        homeView.render();

      });

      app_router.on ( 'route:aboutRoute', function () {

        console.log ( 'about' );
        var aboutView = new AboutView();
        aboutView.render();
      });

      app_router.on ( 'route:galleryRoute', function () {
        console.log('gallery');
        var galleryView = new GalleryView();
        galleryView.render();
      });

      app_router.on ( 'route:galleryTrucksRoute', function () {
        console.log('galleryTrucks');
        var galleryView = new GalleryView();
        galleryView.render('trucks');
      });

      app_router.on ( 'route:galleryHomeRoute', function () {
        console.log('galleryHome');
        var galleryView = new GalleryView();
        galleryView.render('home');
      });

      app_router.on ( 'route:galleryCustomRoute', function () {
        console.log('gallery Custom');
        var galleryView = new GalleryView();
        galleryView.render('custom');
      });

      app_router.on ( 'route:galleryRestorationRoute', function () {
        console.log('gallery restoration');
        var galleryView = new GalleryView();
        galleryView.render('restoration');
      });

      app_router.on ( 'route:galleryLaserRoute', function () {
        console.log('gallery Laser');
        var galleryView = new GalleryView();
        galleryView.render('laser');
      });

      app_router.on ( 'route:contactRoute', function () {
        console.log('contact');
        var contactView = new ContactView();
        contactView.render();

      });

      app_router.on ( 'route:restorationsRoute', function () {
         console.log('restorations');
         var restorationView = new RestorationView();
         restorationView.render();
      });

      app_router.on ( 'route:productsRoute', function () {
        console.log('products');
        var productsView = new ProductView();
        productsView.render();
      });


      //I don't like the hashbang approach, lets use proper url's
      Backbone.history.start ( /*{ pushState: true } lets worry about push state later*/);

    }

    return {
      initialize: initialize
    };

  return {};
});
