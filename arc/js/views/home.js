/**
* Created with dkcustomwelding.com.
* User: lawjeremy
* Date: 2014-03-04
* Time: 03:53 AM
*/
define ( [ 'jquery', 'underscore', 'backbone', 'text!../../templates/home.html', 'bootstrap', 'flipster' ], function ( $, _, Backbone, HomeTemplate, Bootstrap ) {

  var HomeView = Backbone.View.extend({

    tagName: "section",

    className: "",
    el: "section#main",

    template: _.template ( HomeTemplate ),

    events: {
      'click .slider-link': 'advanceSlider'
    },

    advanceSlider: function(evt){


    },

    initialize: function() {
      $('ul.nav li').removeClass('active');
      $('ul.nav li:eq(0)').addClass('active');

    },

    remove: function(){
      //alert('unloading');
    },

    render: function( ) {

      this.$el.html( this.template() );
      _.defer(function(){

        $('.my-flipster').flipster({
          autoplay: 2500,
          loop: true,
          scrollwheel: false
        });

      //  $('.coverflow').coverflow({
			// 		easing:			'easeOutElastic',
			// 		duration:		'slow',
			// 		index:			3,
			// 		width:			320,
			// 		height:			240,
			// 		visible:		'density',
			// 		selectedCss:	{	opacity: 1	},
			// 		outerCss:		{	opacity: .1	}
      //  });

      //   var coverslider = new FWDSimple3DCoverflow({
			// 		//required settings
			// 		coverflowHolderDivId:"coverflow",
			// 		coverflowDataListDivId:"coverflowData",
			// 		displayType:"fluidwidth",
			// 		autoScale:"no",
			// 		coverflowWidth:960,
			// 		coverflowHeight:480,
			// 		skinPath:"load/skin_dk",
      //
			// 		//main settings
			// 		backgroundColor:"",
			// 		backgroundImagePath:"",
			// 		backgroundRepeat:"repeat-x",
			// 		showDisplay2DAlways:"no",
			// 		coverflowStartPosition:"center",
			// 		coverflowTopology:"dualsided",
			// 		coverflowXRotation:0,
			// 		coverflowYRotation:0,
			// 		numberOfThumbnailsToDisplayLeftAndRight:"all",
			// 		infiniteLoop:"no",
			// 		rightClickContextMenu:"default",
			// 		fluidWidthZIndex:700,
      //
			// 		//thumbnail settings
			// 		thumbnailWidth:600,
			// 		thumbnailHeight:400,
			// 		thumbnailXOffset3D:86,
			// 		thumbnailXSpace3D:78,
			// 		thumbnailZOffset3D:200,
			// 		thumbnailZSpace3D:93,
			// 		thumbnailYAngle3D:70,
			// 		thumbnailXOffset2D:20,
			// 		thumbnailXSpace2D:30,
			// 		thumbnailHoverOffset:100,
			// 		thumbnailBorderSize:0,
			// 		thumbnailBackgroundColor:"",
			// 		thumbnailBorderColor1:"",
			// 		thumbnailBorderColor2:"",
			// 		transparentImages:"no",
			// 		thumbnailsAlignment:"center",
			// 		maxNumberOfThumbnailsOnMobile:13,
			// 		showThumbnailsGradient:"no",
			// 		thumbnailGradientColor1:"rgba(0, 0, 0, 0)",
			// 		thumbnailGradientColor2:"rgba(0, 0, 0, 1)",
			// 		showText:"yes",
			// 		textOffset:-410,
			// 		showThumbnailBoxShadow:"no",
			// 		thumbnailBoxShadowCss:"0px 2px 2px #111111",
			// 		showTooltip:"no",
			// 		dynamicTooltip:"yes",
			// 		showReflection:"no",
			// 		reflectionHeight:60,
			// 		reflectionDistance:0,
			// 		reflectionOpacity:.4,
      //
			// 		//controls settings
			// 		slideshowDelay:5000,
			// 		autoplay:"no",
			// 		disableNextAndPrevButtonsOnMobile:"no",
			// 		controlsMaxWidth:700,
			// 		slideshowTimerColor:"#FFFFFF",
			// 		controlsPosition:"bottom",
			// 		controlsOffset:15,
			// 		showPrevButton:"no",
			// 		showNextButton:"no",
			// 		showSlideshowButton:"no",
			// 		showScrollbar:"no",
			// 		disableScrollbarOnMobile:"yes",
			// 		enableMouseWheelScroll:"yes",
			// 		scrollbarHandlerWidth:200,
			// 		scrollbarTextColorNormal:"#000000",
			// 		scrollbarTextColorSelected:"#FFFFFF",
			// 		addKeyboardSupport:"yes",
      //
			// 		//categories settings
			// 		showCategoriesMenu:"no",
			// 		startAtCategory:1,
			// 		categoriesMenuMaxWidth:700,
			// 		categoriesMenuOffset:25,
			// 		categoryColorNormal:"#999999",
			// 		categoryColorSelected:"#FFFFFF",
      //
			// 		//lightbox settings
			// 		addLightBoxKeyboardSupport:"yes",
			// 		showLightBoxNextAndPrevButtons:"yes",
			// 		showLightBoxZoomButton:"yes",
			// 		showLightBoxInfoButton:"yes",
			// 		showLightBoxSlideShowButton:"yes",
			// 		showLightBoxInfoWindowByDefault:"no",
			// 		slideShowAutoPlay:"no",
			// 		lightBoxVideoAutoPlay:"no",
			// 		lightBoxVideoWidth:640,
			// 		lightBoxVideoHeight:480,
			// 		lightBoxIframeWidth:800,
			// 		lightBoxIframeHeight:600,
			// 		lightBoxBackgroundColor:"#000000",
			// 		lightBoxInfoWindowBackgroundColor:"#FFFFFF",
			// 		lightBoxItemBorderColor1:"#fcfdfd",
			// 		lightBoxItemBorderColor2:"#e4FFe4",
			// 		lightBoxItemBackgroundColor:"#333333",
			// 		lightBoxMainBackgroundOpacity:.8,
			// 		lightBoxInfoWindowBackgroundOpacity:.9,
			// 		lightBoxBorderSize:0,
			// 		lightBoxBorderRadius:0,
			// 		lightBoxSlideShowDelay:4000
			// 	});
       });

      //call init to bind handlers etc...
      //initPage();

      return this;
    }

  });

  return HomeView;
});
