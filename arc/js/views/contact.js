/**
* Created with dkcustomwelding.com.
* User: lawjeremy
* Date: 2014-03-22
* Time: 01:21 PM
*/
define( [ 'jquery', 'underscore', 'backbone', 'text!../../templates/contact.html' ], function( $, _, Backbone, ContactView ) {

  var ContactView = Backbone.View.extend({

    tagName: "section",
    el: "section#main",
    classname: "",
    template: _.template ( ContactView ),
    events: {},
    initialize: function(){
      $('ul.nav li').removeClass('active');
      $('ul.nav li:eq(4)').addClass('active');

      $('#coverflow-fluidwidth').remove();
    },
    render: function(){

      this.$el.html( this.template() );

      //initPage();

      return this;
    }
  });

  return ContactView;
});
