/**
* Created with dkcustomwelding.com.
* User: lawjeremy
* Date: 2014-03-22
* Time: 12:21 PM
*/
define ( [ 'jquery', 'underscore', 'backbone', 'text!../../templates/about.html', 'bootstrap' ], function( $, _, Backbone, AboutTemplate ) {

  var AboutView = Backbone.View.extend({
    tagName: "section",
    className: "",
    el: "section#main",
    template: _.template ( AboutTemplate ),
    events: {

    },
    initialize: function() {
      $('ul.nav li').removeClass('active');
      $('ul.nav li:eq(3)').addClass('active');

      $('#coverflow-fluidwidth').remove();

    },
    render: function () {

      this.$el.html( this.template() );

      initPage();
      this.afterRender();
      return this;
    },
    afterRender: function() {
        $('#about-container').addClass('secondary-container-smaller');

    }
  });

  return AboutView;
});
