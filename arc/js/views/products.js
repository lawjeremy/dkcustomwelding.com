/**
* Created with dkcustomwelding.com.
* User: lawjeremy
* Date: 2014-03-22
* Time: 01:52 PM
*/
define ( [ 'jquery', 'underscore', 'backbone', 'text!../../templates/products.html' ], function( $, _, Backbone, ProductTemplate ) {

    var ProductView = Backbone.View.extend({
      tagname: "section",
      classname: "",
      el: "section#main",
      template: _.template ( ProductTemplate ),
      events: {},
      initialize: function(){
        $('ul.nav li').removeClass('active');
        $('ul.nav li:eq(1)').addClass('active');

        $('#coverflow-fluidwidth').remove();
      },
      render: function(){

        this.$el.html(this.template());
        initPage();
        this.afterRender();
        return this;
      },
      afterRender: function() {
        $('.product').addClass('secondary-container-smaller');
      }
    });

    return ProductView;
});
