/**
* Created with dkcustomwelding.com.
* User: lawjeremy
* Date: 2014-03-22
* Time: 01:05 PM
*/
define( ['jquery',
         'underscore',
         'backbone',
         'text!../../templates/galleryTrucks.html',
         'text!../../templates/galleryHome.html',
         'text!../../templates/galleryCustom.html',
         'text!../../templates/galleryRestoration.html',
         'text!../../templates/galleryLaser.html',
         'libs/jquery.anythingslider.min' ],
       function( $,
                  _,
                  Backbone,
                  TrucksTemplate,
                  HomeTemplate,
                  CustomTemplate,
                  RestorationTemplate,
                  LaserTemplate) {

  var GalleryView = Backbone.View.extend({

    tagName: "section",
    el: "section#main",
    classname: "",
    template: null,
    events: {},
    initialize: function(){
      $('ul.nav li').removeClass('active');
      $('ul.nav li:eq(2)').addClass('active');

      var $cffw = $('#coverflow-fluidwidth');
      $cffw.remove();
    },
    render: function (whichTemplate){

      var active;
      var thumbArray;

      //Select gallery to load
      switch (whichTemplate) {
        case 'trucks':

          this.template = _.template ( TrucksTemplate );
          active = 0;
          thumbArray = ['001', '002', '003', '004', '005', '006', '007', '008', '009', '010', '011', '012'];
          break;
        case 'home':
          thumbArray = ['001', '002', '003', '004', '005', '006', '007','008', '009', '010', '011', '012', '013', '014'];
          this.template = _.template ( HomeTemplate );
          active = 1;
          break;
        case 'custom':
          thumbArray = ['001', '002', '003', '004', '005', '006', '007'];
          this.template = _.template ( CustomTemplate );
          active = 2;
          break;
        case 'restoration':
          thumbArray = ['000', '001', '002', '003', '004', '005', '006', '007', '008', '009', '011', '012'];
          this.template = _.template ( RestorationTemplate );
          active = 3;
          break;
        case 'laser':
          thumbArray = ['000', '001', '002', '003', '004', '005'];
          this.template = _.template ( LaserTemplate );
          active = 4;
          break;
        default:
          whichTemplate = 'trucks';
          thumbArray = ['001', '002', '003', '004', '005', '006', '007', '008', '009', '010', '011', '012'];
          active = 0;
          this.template = _.template ( TrucksTemplate );
      }

      //load template
      this.$el.html( this.template() );

      //run init
      //initPage();

      //once gallery is loaded init slider
      _.defer(function(){

        window.scrollTo(0,0);

        $('ul.gallery-nav li').removeClass('active');
        $('ul.gallery-nav li:eq(' + active + ')').addClass('active');

        $('#slider1').anythingSlider({
          theme: false,
          hashTags: false,
          expand: true,
          resizeContents: true,
          aspectRatio: '960:480',
          buildArrows: false,
          navigationFormatter : function(i, panel){
            return '<img src="img/gallery/' + whichTemplate + '/' + thumbArray[i-1] + '.JPG">';
          },
          buildStartStop: false
        });
      });

      return this;
    }
  });


  return GalleryView;
});
