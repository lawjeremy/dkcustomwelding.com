/**
* Created with dkcustomwelding.com.
* User: lawjeremy
* Date: 2016-03-6
* Time: 01:52 PM
*/
define ( [ 'jquery', 'underscore', 'backbone', 'text!../../templates/restorations.html', 'libs/lightgallery-all.min' ], function( $, _, Backbone, RestorationTemplate ) {

    var RestorationView = Backbone.View.extend({
      tagname: "section",
      classname: "",
      el: "section#main",
      template: _.template ( RestorationTemplate ),
      events: {},
      initialize: function(){
        $('ul.nav li').removeClass('active');
        $('ul.nav li:eq(1)').addClass('active');

        $('#coverflow-fluidwidth').remove();
      },
      render: function(){

        this.$el.html(this.template());


        //once gallery is loaded init slider
        _.defer(function(){

          $("#lightgallery").lightGallery({
            "download": false
          });


        });
        return this;
      }
    });

    return RestorationView;
});
