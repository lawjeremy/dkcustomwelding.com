module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {                              // Task
      dist: {                            // Target
        options: {                       // Target options
          style: 'expanded'
        },
        files: {                            // Dictionary of files
          'css/style.css': 'css/style.scss' // 'destination': 'source'
        }
      }
    },
    watch: {
      scripts: {
        files: ['**/*.scss'],
        tasks: ['sass'],
        options: {
          spawn: false,
          debounceDelay: 1000
        }
      },
      livereload: {
        // Here we watch the files the sass task will compile to
        // These files are sent to the live reload server after sass compiles to them
        options: { livereload: true },
        files: ['css/*.css'],
      }
    },
    copy: {
      main: {
        files: [
          // includes files within path
          {expand: true,
           cwd: "bower_components/bootstrap/dist/css/",
           src: "bootstrap.css",
           dest: 'css/'
          },
          {expand: true,
           cwd: "bower_components/bootstrap/js/",
           src: "carousel.js",
           dest: 'js/libs/'
          },
          {expand: true,
           cwd: "bower_components/bootstrap/dist/js/",
           src: "bootstrap-responsive.min.css",
           dest: 'css/'
          },
          {expand: true,
           cwd: 'bower_components/flatstrap/assets/js/',
           src: 'bootstrap.min.js',
           dest: 'js/libs/'
          },
          {expand: true,
           cwd: 'bower_components/requirejs/',
           src: 'require.js',
           dest: 'js/libs/'
          },
          {expand: true,
           cwd: 'bower_components/underscore/',
           src: 'underscore.js',
           dest: 'js/libs/'
          },
          {expand: true,
           cwd: 'bower_components/backbone/',
           src: 'backbone.js',
           dest: 'js/libs/'
          },
          {expand: true,
           cwd: 'bower_components/jquery/',
           src: 'jquery.js',
           dest: 'js/libs/'
          },
          {expand: true,
           cwd: 'bower_components/jquery-maskedinput/src/',
           src: 'jquery.maskedinput.js',
           dest: 'js/libs/'
          },
          {expand: true,
           cwd: 'bower_components/requirejs-text/',
           src: 'text.js',
           dest: 'js/libs/'
          },
          {expand: true,
           cwd: 'bower_components/AnythingSlider/js/',
           src: 'jquery.anythingslider.min.js',
           dest: 'js/libs/'
          },
          {expand: true,
           cwd: 'bower_components/AnythingSlider/css/',
           src: 'anythingslider.css',
           dest: 'css/'},
           {expand: true,
            cwd: 'bower_components/coverflowjs/dist/',
            src: 'coverflow.js',
            dest: 'js/libs'},
          {expand: true,
             cwd: 'bower_components/coverflowjs/dist/',
             src: 'coverflow.css',
             dest: 'css/'},
          {expand: true,
              cwd: 'bower_components/jquery-ui/',
              src: 'jquery-ui.min.js',
              dest: 'js/libs'},
          {expand: true,
             cwd: 'bower_components/jquery-flipster/dist/',
             src: 'jquery.flipster.min.css',
             dest: 'css/'},
          {expand: true,
              cwd: 'bower_components/jquery-flipster/dist/',
              src: 'jquery.flipster.min.js',
              dest: 'js/libs'},
           {expand: true,
               cwd: 'bower_components/lightgallery/dist/css/',
               src: 'lightgallery.css',
               dest: 'css'},
            {expand: true,
                cwd: 'bower_components/lightgallery/dist/js/',
                src: 'lightgallery-all.min.js',
                dest: 'js/libs'}


        ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask('default', ['sass','watch', 'copy']);


};
