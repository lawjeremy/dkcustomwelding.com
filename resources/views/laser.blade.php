@extends('template')

@section('title', 'Laser Cutting')

@section('content')
<div class="row secondary-container product">
  <div class="row secondary-inner">
    <div class="secondary-hero">

      <img src="/img/gallery/2016-update/slider/laser.jpg" class="laser-hero" />

    </div>
  </div>
</div>

  <div class="row tertiary-container">
    <div class="row tertiary-inner">
      <h1>Laser Cutting</h1>
        <p>Whether it’s a small, intricate medical device or a large assembly component, the capability of our two Mitsubishi  4000watt CNC lasers and their highly skilled operators who are experience in problem solving of difficult projects, will certainly offer the satisfaction our customer demand.</p>

        <p>Combine this with our two 60” x 120” state of the art cutting beds that cut to 1” mild steel, or 3/8” stainless and aluminum.  From prototype to full on production, we deliver professional, accurate and high quality results every time and at very competitive rates!</p>

        <p>Some projects we are experienced in cutting are:</p>
          <ul>
            <li>Production cutting for industrial needs</li>
            <li>Prototype Components</li>
            <li>Car Parts</li>
            <li>Decorative Wall Art</li>
            <li>Company Logos and Business Signs</li>
          </ul>

        <p>Call or e-mail us for a quote today!</p>

        <p class="gallery-btn">View image gallery</p>
 </div>
  </div>


@endsection

@section('javascript')
  <script>
      $('ul.nav li').removeClass('active');
      $('ul.nav li:eq(1)').addClass('active');

      $(".gallery-btn").on('click', function(e){

        const image_dir = 'img/gallery/2016-update/laser_cutting/';

        let image_array = getImageArray(image_dir, 3);
        console.log(image_array);

        $(this).lightGallery({
          "dynamic": true,
          "dynamicEl": image_array,
          "download": false,
          "thumbnail": true
        });

      });




  </script>
@endsection
