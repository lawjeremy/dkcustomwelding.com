@extends('template')

@section('title', 'About Us')

@section('content')


<div class="row tertiary-container">

  <div class="row tertiary-inner">
    <h1>About DK Custom Welding</h1>
     <p>Owner &amp; Operator Dave Krabbe started out in 2005, providing rust protection coating for many vehicle types, including passenger, commercial and farming vehicles. With a few years of experience under his belt, Dave decided to branch out and offer services in custom welding and fabrication. This move seemed natural with 13 years of aluminum fabrication already on his resume, and DK Custom Welding &amp; Design was born.</p>

     <p>Over the years, DK Custom Welding &amp; Design has completed jobs of all shapes and sizes, and considers no job too big or too small. An eagerness to take on any and all challenges, has allowed DK to successfully and rapidly expand.</p>

     


  </div>

</div>
@endsection

@section('javascript')
  <script>
      $('ul.nav li').removeClass('active');
      $('ul.nav li:eq(7)').addClass('active');
  </script>
@endsection
