@extends('template')

@section('title', 'Custom Home Upgrades')

@section('content')
<div class="row secondary-container product">
  <div class="row secondary-inner">
    <div class="secondary-hero">

      <img src="/img/gallery/2016-update/slider/home.jpg" class="laser-hero" />

    </div>
  </div>
</div>

  <div class="row tertiary-container">
    <div class="row tertiary-inner">
      <h1>Custom Home Upgrades</h1>
        <p>DK is proud of the portfolio we have to show of all the unique home upgrade services we have provided our customers with. A few projects to mention are:</p>
        <ul>
          <li>Stainless steel countertops for residential and commercial uses.</li>
          <li>Stainless Steel railings custom designed for a home.</li>
          <li>Aluminum Deck Railings</li>
          <li>Stainless Steel Fireplace Mantel</li>
          <li>Aluminum Bookcases for hidden doorways</li>
          <li>Aluminum Gable Ends</li>
          <li>Custom Door tracks for sliding barn doors</li>
          <li>Garage Staircase</li>
          <li>Stainless Steel Kitchen Island</li>
          <li>Custom Decorative Wall Art</li>
          <li>Family Lawn Signs</li>
          <li>Spiral Staircases</li>
          <li>Custom Security Bars and Grating for Windows and Entrances</li>
          <li>Personalize Fire Pit Surrounds</li>
        </ul>
        <p>As custom fabricators we take your design and make it all in-house.  We can work with a personal design or help create something for you.  Our qualified team can help optimize your vision. </p>
    </div>
  </div>


@endsection

@section('javascript')
  <script>
      $('ul.nav li').removeClass('active');
      $('ul.nav li:eq(1)').addClass('active');

      $(".gallery-btn").on('click', function(e){

        const image_dir = 'img/gallery/2016-update/laser_cutting/';

        let image_array = getImageArray(image_dir, 3);
        console.log(image_array);

        $(this).lightGallery({
          "dynamic": true,
          "dynamicEl": image_array,
          "download": false,
          "thumbnail": true
        });

      });




  </script>
@endsection
