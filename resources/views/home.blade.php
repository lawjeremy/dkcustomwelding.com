@extends('template')

@section('title', 'DK Custom Welding &amp; Design')

@section('content')
<div class="row secondary-container product">
  <div class="row secondary-inner">
    <div class="secondary-hero hide-home">

      <div class="my-flipster" style="margin-top: 20px;">
        <ul>


        
          <!-- HOME AND OFFICE -->
          <li>
            <div style="width: 750px; height: 400px; background: url('/img/home-5.jpg');">
              <div class="slider-text">
                <div class="handle">
                 <h3>Home Upgrades</h3>
                </div>
                <div class="body">
                  <p>From decorative interior stair railings to functional pool fencing, countertops, and backsplashes, aluminum parts can add a strong and beautiful element to your home. Requiring little maintenance, aluminum and stainless are great alternatives for in home structures.</p>

                  <p>Every in home piece is custom designed and manufactured for you and your home to showcase individual styles and assure a quality result that fits you and your home perfectly.</p>

                  <p></p>

                  <p><a href="/#/gallery/home">View image gallery</a></p>


                </div>
              </div>
            </div>
          </li>

          <!-- CUSTOM FABRICATION -->
          <li>
            <div style="width: 750px; height: 400px; background: url('/img/custom.JPG');">
              <div class="cover slider-text">
                <div class="handle">
                 <h3 class="">Custom Fabrication</h3>
                </div>
                <div class="body">
                  <p>Whether it's aluminum, stainless or carbon steel, we provide mig and tig welding services, fabrication, laser cutting and machine shop capabilities. Engineering and design consultations are also available. </p>

                  <p>Always looking for a challenge, we would be happy to help you design your perfect product or assist with your production needs.  Whether the project is big or small, DK Custom Welding &amp; Design will meet your needs and exceed your expectations.</p>

                  <p><a href="/#/gallery/custom">View image gallery</a></p>

                </div>
              </div>
            </div>
          </li>


        </ul>
      </div>

    </div>
  </div>
</div>

<div class="row tertiary-container">
  <div class="row tertiary-inner">

  <div class="col-md-9">
    <h1>DK Custom Welding</h1>
    <p>DK Custom Welding and Design has both the capacity and ability to take your project from concept to completion.They provide superior quality aluminum, carbon and stainless steel fabrication services, custom truck accessories and work with both homeowners and industry professionals to create unique custom upgrades.
    They are also proud to offer a full vehicle restoration and custom Hotrods shop.If you have a project in mind, drop in today for a free quote.</p>
  </div>

    <!-- <div class="col-md-1"></div><div class="col-md-1"></div> -->
    <div class="col-md-2">
      <div style="height: 30px;"></div>
      <h1>
        <a href="mailto:info@dkcustomwelding.com?subject=Quote request">
          <img src="/img/btn-quote.png" class="btn-contact" />
        </a>
      </h1>

      <h1>
        <a href="tel:5198469085">
          <img src="/img/btn-phone.png" class="btn-contact" />
        </a>
      </h1>
    </div>
  </div>


</div>

@endsection

@section('javascript')
  <script>
    $('ul.nav li').removeClass('active');
    $('ul.nav li:eq(0)').addClass('active');
  </script>
@endsection
