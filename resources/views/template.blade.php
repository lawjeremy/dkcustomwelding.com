<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title')</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


      <link rel="stylesheet" href="/css/bootstrap.css">
      <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
      <link rel="stylesheet" href="/css/anythingslider.css">
      <link rel="stylesheet" href="/css/jquery.flipster.min.css" />
      <link rel="stylesheet" href="/css/lightgallery.css" />

      <link rel="stylesheet" href="/css/style.css" />
      <link rel="stylesheet" href="/css/header.css" />


    </head>
    <body>
      <header>
        <div id="nav-wrap">

          
            <div id="dk-logo" class="logo-container">
              <img class="img-responsive" src="/img/dk-welding-on-plate-straight.png" />
            </div>

            <nav class="navbar" role="navigation">

              <ul class="nav nav-pills">
                <li class="active nav-first"><a href="/">Home</a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">What We Do</a>
                  <ul class="dropdown-menu">
                    <li><a href="/home-upgrades">Custom Home Upgrades</a></li>
                    <!--<li><a href="/gallery">Trucks</a></li>-->
                    <li><a href="/custom-fabrication">Custom Fabrication</a></li>
                  </ul>
                </li>
                <li><a href="/gallery">Gallery</a></li>
                <li><a href="/about">About</a></li>
                <li><a href="/contact">Contact</a></li>
              </ul>
            </nav>  
          
            <div id="customs-logo" class="logo-container">
              <img class="img-responsive" src="/img/dk-home-on-plate-straight.png" />
            </div>
          
                  
        </div>
      </header>

       <section role="main" id="main" class="container-fluid main-container">

         @yield('content')

       </section>
       <footer class="">
          <div class="clearfix footer-inner">
            <div cass="row">
              <div class="col-md-3">
                <h4>DK Custom Welding</h4>

                <h5><a href="tel:519-820-2000">519-820-2000</a></h5>
                <h5><a href="mailto:info@dkcustomwelding.com?subject=website inquiry">info@dkcustomwelding.com</a></h5>
                <!-- <a href="mailto:info@dkcustomwelding.com?subject=Quote request"><img src="/img/btn-quote.png" class="btn-quote-footer" /></a> -->
                <div class="social">
                  <a href="https://www.facebook.com/DkCustomWeldingDesign" class="social-box facebook"><i class="fa fa-facebook"></i></a>
                  <a href="https://twitter.com/DKcwandDesign" class="social-box twitter"><i class="fa fa-twitter"></i></a>
                  <a href="https://www.pinterest.com/dkcustomwelding/" class="social-box pinterest"><i class="fa fa-pinterest"></i></a>
                  <a href="https://www.instagram.com/dkcustomweldinganddesign" class="social-box instagram"><i class="fa fa-instagram"></i></a>
                </div>
              </div>

              <div class="col-md-9">
               <p class="footer-right visible-lg visible-md">DK Custom Welding and Design provides aluminum, carbon and stainless steel fabrication services..including home upgrades and top quality vehicle accessories. Have something in mind...let us know, and we can custom fabricate it!</p>
               <p class="copyright">&copy; <script>document.write(new Date().getFullYear());</script> DK Custom Welding</p>

              </div>
            </div>
          </div>
       </footer>
      <script src="/node_modules/jquery/dist/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <script src="/node_modules/lightgallery/dist/js/lightgallery.min.js"></script>
      <script src="/js/lg-thumbnail.js"></script>
      <script src="/js/jquery.anythingslider.min.js"></script>
      <script src="/js/jquery.flipster.js"></script>
      <script src="/js/main.js"></script>

        @yield('javascript')

      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-10150020-10', 'auto');
        ga('send', 'pageview');

      </script>

     </body>
</html>
