@extends('template')

@section('title', 'Contact Us')

@section('content')
<div class="row secondary-container">
  <div class="row secondary-inner">
    <div class="secondary-hero contact">
      <div class="google-maps">
       <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d16752.378999803335!2d-80.40620791807484!3d43.68049563403586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x882b959dd0c2ff2d%3A0xce64c85710aaa2ac!2s1+Line%2C+Elora%2C+ON+N0B!5e0!3m2!1sen!2sca!4v1505439765792" width="600" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>



  </div>
</div>

<div class="row tertiary-container">
  <div class="row tertiary-inner">

    <div class="col-md-9">
      <h1>Contact Us</h1>

      <p>Looking for a custom quote? </p>
      <p>Have a question about one of our products?  Call, or email</p>

    </div>

    <div class="col-md-2">
      <h1><a href="mailto:info@dkcustomwelding.com?subject=Quote request"><img src="/img/btn-quote.png" class="btn-contact" /></a></h1>

      <h1><a href="tel:5198202000"><img src="/img/btn-phone.png" class="btn-contact" /></a></h1>
    </div>

  </div>
</div>

@endsection

@section('javascript')
  <script>
      $('ul.nav li').removeClass('active');
      $('ul.nav li:eq(8)').addClass('active');
  </script>
@endsection
