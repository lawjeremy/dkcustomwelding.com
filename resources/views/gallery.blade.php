@extends('template')

@section('title', 'Galleries')

@section('content')
<div class="row secondary-container product">
  <div class="row secondary-inner">
    <div class="secondary-hero">

      <div id="lightgallery-one">
        <div class="col-sm-4 col-md-4 gallery-preview">
          <div class="body">
            <img src="/img/gallery/2016-update/gallery_previews/home.jpg" class="img-responsive" style="width: 100%;" />
          </div>
          <div class="handle">
            <h5>Home</h5>
          </div>

        </div>
      </div>

      <div id="lightgallery-two">
        <div class="col-sm-4 col-md-4 gallery-preview">
          <div class="body">
            <img src="/img/gallery/2016-update/gallery_previews/trucks.jpg" class="img-responsive" style="width: 100%;" />
          </div>
          <div class="handle">
            <h5>Trucks</h5>
          </div>

        </div>
      </div>

      <div id="lightgallery-three">
        <div class="col-sm-4 col-md-4 gallery-preview">
          <div class="body">
            <img src="/img/gallery/2016-update/gallery_previews/cabover.jpg" class="img-responsive" style="width: 100%;" />
          </div>
          <div class="handle">
            <h5>1941 Ford Cabover</h5>
          </div>

        </div>
      </div>

      <div id="lightgallery-four">
        <div class="col-sm-4 col-md-4 gallery-preview">
          <div class="body">
            <img src="/img/gallery/2016-update/gallery_previews/hummer.jpg" class="img-responsive" style="width: 100%;" />
          </div>
          <div class="handle">
            <h5>1985 H1 Military Hummer</h5>
          </div>

        </div>
      </div>


      <div id="lightgallery-five">
        <div class="col-sm-4 col-md-4 gallery-preview">
          <div class="body">
            <img src="/img/gallery/2016-update/gallery_previews/fabrication.jpg" class="img-responsive" style="width: 100%;" />
          </div>
          <div class="handle">
            <h5>Custom Fabrication</h5>
          </div>

        </div>
      </div>


   </div>
 </div>
</div>

<div class="row tertiary-container">
  <div class="row tertiary-inner">
    <h1>Image Gallery</h1>
    <p>Click a category above to view the image gallery</p>
  </div>

</div>
@endsection

@section('javascript')
  <script>
      $('ul.nav li').removeClass('active');
      $('ul.nav li:eq(6)').addClass('active');

      $("#lightgallery-one").on('click', function(e){

        const image_dir = 'img/gallery/2016-update/home_office/';

        let image_array = getImageArray(image_dir, 24);
        console.log(image_array);

        $(this).lightGallery({
          "dynamic": true,
          "dynamicEl": image_array,
          "download": false,
          "thumbnail": true
        });

      });

      $("#lightgallery-two").on('click', function(e){

        const image_dir = 'img/gallery/2016-update/trucks/';

        let image_array = getImageArray(image_dir, 30);
        console.log(image_array);

        $(this).lightGallery({
          "dynamic": true,
          "dynamicEl": image_array,
          "download": false,
          "thumbnail": true
        });

      });

      $("#lightgallery-three").on('click', function(e){

        const image_dir = 'img/gallery/2016-update/restoration/cabover/';

        let image_array = getImageArray(image_dir, 11);
        console.log(image_array);

        $(this).lightGallery({
          "dynamic": true,
          "dynamicEl": image_array,
          "download": false,
          "thumbnail": true
        });

      });

      $("#lightgallery-four").on('click', function(e){

        const image_dir = 'img/gallery/2016-update/restoration/hummer/';

        let image_array = getImageArray(image_dir, 5);
        console.log(image_array);

        $(this).lightGallery({
          "dynamic": true,
          "dynamicEl": image_array,
          "download": false,
          "thumbnail": true
        });

      });

      $("#lightgallery-five").on('click', function(e){

        const image_dir = 'img/gallery/2016-update/custom_fabrication/';

        let image_array = getImageArray(image_dir, 5);
        console.log(image_array);

        $(this).lightGallery({
          "dynamic": true,
          "dynamicEl": image_array,
          "download": false,
          "thumbnail": true
        });

      });
  </script>
@endsection
