@extends('template')

@section('title', 'Restorations')

@section('content')
<div class="row secondary-container">
  <div class="row secondary-inner">
    <div class="secondary-hero">

      <div id="lightgallery-one">
        <div class="col-sm-4 col-md-4 gallery-preview">
          <div class="body">
            <img src="/img/gallery/2016-update/gallery_previews/cabover.jpg" class="img-responsive" style="width: 100%;" />
          </div>
          <div class="handle">
            <h5>1941 Ford Cabover</h5>
          </div>

        </div>
      </div>

      <div id="lightgallery-two">
        <div class="col-sm-4 col-md-4 gallery-preview">
          <div class="body">
            <img src="/img/gallery/2016-update/gallery_previews/hummer.jpg" class="img-responsive" style="width: 100%;" />
          </div>
          <div class="handle">
            <h5>1985 H1 Military Hummer</h5>
          </div>

        </div>
      </div>

      <div id="lightgallery-three">
        <div class="col-sm-4 col-md-4 gallery-preview">
          <div class="body">
            <img src="/img/gallery/2016-update/gallery_previews/40-chev.jpg" class="img-responsive" style="width: 100%;" />
          </div>
          <div class="handle">
            <h5>1940 Chevy Pickup</h5>
          </div>

        </div>
      </div>

      <div id="lightgallery-four">
        <div class="col-sm-4 col-md-4 gallery-preview">
          <div class="body">
            <img src="/img/gallery/2016-update/gallery_previews/55-chev.jpg" class="img-responsive" style="width: 100%;" />
          </div>
          <div class="handle">
            <h5>1955 Chevy Pickup</h5>
          </div>

        </div>
      </div>

      <div id="lightgallery-five">
        <div class="col-sm-4 col-md-4 gallery-preview">
          <div class="body">
            <img src="/img/gallery/2016-update/gallery_previews/69-sat.jpg" class="img-responsive" style="width: 100%;" />
          </div>
          <div class="handle">
            <h5>1967 Satellite</h5>
          </div>

        </div>
      </div>

      <div id="lightgallery-six">
        <div class="col-sm-4 col-md-4 gallery-preview">
          <div class="body">
            <img src="/img/gallery/2016-update/gallery_previews/charger.jpg" class="img-responsive" style="width: 100%;" />
          </div>
          <div class="handle">
            <h5>1970 Charger</h5>
          </div>

        </div>
      </div>


    </div>

    </div>
  </div>

  <div class="row tertiary-container">
    <div class="row tertiary-inner">
      <h1>Restorations</h1>
      <p>From Concept to completion we can help you bring your hot rod vision to life.  DK Custom Welding &amp; Design offers a full restoration shop and is able to deliver all services needed to restore or build a vehicle to your specifications.</p>

      <p>From metal work to paint, we have the right tools and expertise to take on your project. DK offers total restoration of collective, classic, muscle machines, street rods and special interest automobiles.  We strive to provide the best service and highest quality workmanship.</p>

      <p></p>


 </div>
  </div>

@endsection

@section('javascript')
  <script>
      $('ul.nav li').removeClass('active');
      $('ul.nav li:eq(1)').addClass('active');

      $("#lightgallery-one").on('click', function(e){

        const image_dir = 'img/gallery/2016-update/restoration/cabover/';

        let image_array = getImageArray(image_dir, 26);

        $("#lightgallery-one").lightGallery({
          "dynamic": true,
          "dynamicEl": image_array,
          "download": false,
          "thumbnail": true
        });

      });

      $("#lightgallery-two").on('click', function(e){

        const image_dir = 'img/gallery/2016-update/restoration/hummer/';

        let image_array = getImageArray(image_dir, 5);
        console.log(image_array);

        $("#lightgallery-two").lightGallery({
          "dynamic": true,
          "dynamicEl": image_array,
          "download": false,
          "thumbnail": true
        });

      });

      $("#lightgallery-three").on('click', function(e){

        const image_dir = 'img/gallery/2016-update/restoration/1940-chevrolet-pickup/';

        let image_array = getImageArray(image_dir, 5);
        console.log(image_array);

        $("#lightgallery-three").lightGallery({
          "dynamic": true,
          "dynamicEl": image_array,
          "download": false,
          "thumbnail": true
        });

      });

      $("#lightgallery-four").on('click', function(e){

        const image_dir = 'img/gallery/2016-update/restoration/1955-chevrolet-pickup/';

        let image_array = getImageArray(image_dir, 4);
        console.log(image_array);

        $("#lightgallery-four").lightGallery({
          "dynamic": true,
          "dynamicEl": image_array,
          "download": false,
          "thumbnail": true
        });

      });

      $("#lightgallery-five").on('click', function(e){

        const image_dir = 'img/gallery/2016-update/restoration/1969-satellite/';

        let image_array = getImageArray(image_dir, 1);
        console.log(image_array);

        $("#lightgallery-five").lightGallery({
          "dynamic": true,
          "dynamicEl": image_array,
          "download": false,
          "thumbnail": true
        });

      });

      $("#lightgallery-six").on('click', function(e){

        const image_dir = 'img/gallery/2016-update/restoration/1970-charger/';

        let image_array = getImageArray(image_dir, 10);
        console.log(image_array);

        $("#lightgallery-six").lightGallery({
          "dynamic": true,
          "dynamicEl": image_array,
          "download": false,
          "thumbnail": true
        });

      });



  </script>
@endsection
