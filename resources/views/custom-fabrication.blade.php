@extends('template')

@section('title', 'Custom Fabrication')

@section('content')
<div class="row secondary-container product">
  <div class="row secondary-inner">
    <div class="secondary-hero">

      <img src="/img/gallery/2016-update/slider/fabrication.jpg" class="laser-hero" />

    </div>
  </div>
</div>

  <div class="row tertiary-container">
    <div class="row tertiary-inner">
      <h1>Custom Fabrication</h1>
        <p>Whether it's aluminum, stainless or carbon steel, we provide mig and tig welding services, and fabrication.  Engineering and design consultations are also available.</p>

        <p>Always looking for a challenge, we would be happy to help you design your perfect product or assist with your production needs.  Whether the project is big or small, DK Custom Welding &amp; Design will meet your needs and exceed your expectations.</p>
 </div>
  </div>


@endsection

@section('javascript')
  <script>
      $('ul.nav li').removeClass('active');
      $('ul.nav li:eq(1)').addClass('active');

      $(".gallery-btn").on('click', function(e){

        const image_dir = 'img/gallery/2016-update/laser_cutting/';

        let image_array = getImageArray(image_dir, 3);
        console.log(image_array);

        $(this).lightGallery({
          "dynamic": true,
          "dynamicEl": image_array,
          "download": false,
          "thumbnail": true
        });

      });




  </script>
@endsection
