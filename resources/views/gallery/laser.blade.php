@extends('template')

@section('title', 'Restorations')

@section('content')
<div class="row secondary-container">
  <div class="row secondary-inner">
    <div class="secondary-hero">

      <div id="lightgallery-one">
        <div class="col-sm-4 col-md-4 feature feature-product">
          <div class="body">
            <img src="/img/dk-cover-7.png" class="img-responsive" style="width: 100%;" />
          </div>
          <div class="handle">
            <h5>Heavy Duty Tonneau Cover<br />(Patent Pending)</h5>
          </div>

        </div>
      </div>

      <div id="lightgallery-two">
        <div class="col-sm-4 col-md-4 feature feature-product">
          <div class="body">
            <img src="/img/dk-cover-7.png" class="img-responsive" style="width: 100%;" />
          </div>
          <div class="handle">
            <h5>Heavy Duty Tonneau Cover<br />(Patent Pending)</h5>
          </div>
        </div>
      </div>

      <div id="lightgallery-three">
         <div class="col-sm-4 col-md-4 feature feature-product">
           <div class="body">
             <img src="/img/dk-cover-7.png" class="img-responsive" style="width: 100%;" />
           </div>
           <div class="handle">
             <h5>Heavy Duty Tonneau Cover<br />(Patent Pending)</h5>
           </div>
         </div>
       </div>


    </div>

    </div>
  </div>

  <!-- <div class="row tertiary-container">

  </div> -->

@endsection

@section('javascript')
  <script>
      $('ul.nav li').removeClass('active');
      $('ul.nav li:eq(1)').addClass('active');

      $("#lightgallery-one").on('click', function(e){

        const image_dir = '/img/gallery/restoration/';

        $("#lightgallery-one").lightGallery({
          "dynamic": true,
          "dynamicEl": [
            {'src': image_dir + '0.jpg'},
            {'src': image_dir + '1.jpg'},
            {'src': image_dir + '2.jpg'}
          ],
          "download": false,
          "thumbnail": true
        });

      });

      $("#lightgallery-two").on('click', function(e){

        const image_dir = '/img/gallery/2016-update/custom_fabrication/';

        let image_array = getImageArray(image_dir, 3);
        console.log(image_array);

        $("#lightgallery-two").lightGallery({
          "dynamic": true,
          "dynamicEl": image_array,
          "download": false,
          "thumbnail": true
        });

      });

      $("#lightgallery-three").on('click', function(e){

        const image_dir = '/img/gallery/2016-update/laser_cutting/';

        let image_array = getImageArray(image_dir, 5);
        console.log(image_array);

        $("#lightgallery-three").lightGallery({
          "dynamic": true,
          "dynamicEl": image_array,
          "download": false,
          "thumbnail": true
        });

      });


  </script>
@endsection
