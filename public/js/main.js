//bind event handlers
$( document ).bind ( 'scroll', function () {

  var top = $( document ).scrollTop();
  var width = window.innerWidth;
  //console.log ( width );

  // if ( top > 100 && width > 768 ) {

  //   $( 'header' ).addClass ( 'header-reduced' );
  //   $( '.gallery-sub-nav' ).addClass ( 'high' );
  //   $( '.header-logo' ).addClass ( 'small' );
  // }

  // if ( top == 0 ) {

  //   $( 'header' ).removeClass ( 'header-reduced' );
  //   $( '.gallery-sub-nav' ).removeClass ( 'high' );
  //   $( '.header-logo' ).removeClass ( 'small' );
  // }

});

$('#main-carousel').anythingSlider({
    autoPlay: true,
    theme: false,
    hashTags: false,
    buildArrows: true,
    buildStartStop: false,
    buildNavigation: false,
    aspectRatio: '630:480',
    expand: true
  });

$('.my-flipster').flipster({
  autoplay: 5000,
  loop: true,
  scrollwheel: false
});

function getImageArray(image_dir, num_images)
{
  let image_array = [];
  for(let i = 0; i <= num_images - 1; i++ )
  {
    image_array[i] = {'src': image_dir + (i+1) + '.jpg', 'thumb': image_dir + (i+1) + '.jpg'};
  }

  return image_array;
}
