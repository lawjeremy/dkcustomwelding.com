module.exports = function(grunt) {

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    sass: {
      options: {
        sourceMap: true
      },
      dist: {
        files: {
          'css/style.css': 'css/style.scss',
          'css/header.css': 'css/header.scss'
        }
      }
    },

    postcss: {
      options: {
        map: true, // inline sourcemaps
        processors: [
          require('autoprefixer')({browsers: ['last 2 versions', 'ie >= 9']}),
          require('cssnano')()
        ]
      },
      dist: {
        src: ['css/style.css', 'css/header.css']
      }
    },

    watch: {
      scripts: {
        files: ['**/*.scss'],
        tasks: ['sass', 'postcss'],
        options: {
          spawn: false,
        },
      },
    }
  });

  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['watch']);
};